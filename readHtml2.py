from bs4 import BeautifulSoup
import xlsxwriter
import datetime
import sys
import traceback
#import createExcel as ex
def create_excel(x,y):
	today=datetime.date.today().strftime("%m%d%Y")
	workbook=xlsxwriter.Workbook('Vanguard_iRequest_'+today+'.xlsx')
	worksheet=workbook.add_worksheet()
	bold = workbook.add_format({'bold': True})
	rowm=0
	colm=0
	row =1
	for h in x:
		worksheet.write(rowm,colm,h,bold)
		colm+=1

	for n in y:
		col=0
		for q in n:
			worksheet.write(row,col,q)
			col+=1
		row+=1
	workbook.close()

try:
	html_text = open('vanguard_IREQUEST.html').read();
	soup = BeautifulSoup(html_text,"lxml")
	table = soup.find("table")
	headings= [th.get_text() for th in table.find("tr").find_all("th")]
	datasets=[]
	for row in table.find_all("tr")[1:]:
		dataset =[td.get_text() for td in row.find_all("td")]
		datasets.append(dataset)
	print("data done")
	create_excel(headings,datasets);
	print("Excel created")
except Exception as e:
	print e
	traceback.print_exc()
	print "Error in Html File"
	sys.exit(1);


